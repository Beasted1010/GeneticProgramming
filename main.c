

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define POOL_SIZE 80
#define MUTATION_RATE 0.3
#define UPPERCASE 65
#define LOWERCASE 97
#define AVG_SPACE_GAP 10

#define SURVIVOR_RATE 0.08
#define SURVIVOR_THREASH 4

#define PROB_MATE 0.6
#define PROB_SUCC_REPRODUCTION 0.7

// TODO: Don't have so many entities resulting in mating pool (Pool grows exponentially quickly...)
// TODO: Inefficiencies... Pretty sure I am going way more than necessary.

// TODO: This is broken for many other strings
//const char TARGET[] = "A string that says nothing important so lets rock";
const char TARGET[] = "Bob is a great individual with food and dreams";
int STRING_LENGTH = 0;
int TARGET_FITNESS = 0;

clock_t START_TIME;

typedef struct Entity {
    char* sentence; // The characters are the entity's "genes"
    int fitness_score;
} Entity;

typedef struct Pool {
    Entity* entities;
    int num_entities;

    int survivor_threash_hold;
    int survivor_rate;

    int generation;
    double mutation_rate;
} Pool;

int PrettyFormat();
void RandomString(int length, char** str);
Pool* CreateInitialPool(int poolSize);
void DeepCopyPool(Pool* dest, Pool* source);
int PrintPool(Pool* pool, const char* name);
void PrintPoolStatistics(Pool* pool, const char* name);
void DestroyPool(Pool* pool);
void AssignFitnessValues(Pool* pool);
int SelectSurvivors(Pool* pool, Entity** out_survivors);
int ReproduceSurvivors(Pool* pool);
Entity MateEntities(Pool* pool, Entity* e1, Entity* e2);

void ValidateObject(void* ptr, const char* message)
{
    if(!ptr)
    {
        printf("Failed to allocate memory! %s\n", message);
        exit(1);
    }
}

int main(int argc, char** argv)
{
    STRING_LENGTH = strlen(TARGET) + 1;
    TARGET_FITNESS = STRING_LENGTH - 1;
    START_TIME = clock();
    srand(time(NULL));
    
    Pool* pool = CreateInitialPool(POOL_SIZE);
    AssignFitnessValues(pool);

    PrintPool(pool, "Initial");

    while( !ReproduceSurvivors(pool) )
    {
        if(PrintPool(pool, "NewGen"))
        {
            break;
        }
    }

    //PrintPoolStatistics(pool, "Final");

    DestroyPool(pool);
    return 0;
}



Pool* CreateInitialPool(int poolSize)
{
    Pool* pool = malloc(sizeof(Pool));
    ValidateObject(pool, "pool");

    pool->num_entities = poolSize;
    pool->entities = malloc(sizeof(Entity) * pool->num_entities);
   
    ValidateObject(pool->entities, "pool->entities");
    
    pool->mutation_rate = MUTATION_RATE;

    // TODO: Design what to do with this calculation
    pool->survivor_rate = SURVIVOR_RATE;
    //pool->survivor_threash_hold = pool->num_entities * SURVIVOR_RATE;
    pool->survivor_threash_hold = SURVIVOR_THREASH;

    char* str = malloc(sizeof(char)); 
    ValidateObject(str, "str");

    if(!str)
    {
        printf("Failed to allocate memory for the string!\n");
        exit(1);
    }

    int str_len = STRING_LENGTH;

    for(int i = 0; i < pool->num_entities; i++)
    {
        RandomString(str_len, &str);

        pool->entities[i].sentence = malloc(sizeof(char) * str_len);
        ValidateObject(pool->entities[i].sentence, "pool->entities[i].sentence");

        pool->entities[i].fitness_score = 0;

        strcpy(pool->entities[i].sentence, str);
    }

    pool->generation = 1;

    free(str);
    return pool;
}

int PrettyFormat()
{
    printf("\n------------------------------------------------------------------\n");
}

void RandomString(int length, char** str)
{
    if(!(*str))
    {
        printf("The string parameter value can't be NULL!\n");
        exit(1);
    }
    int str_size = 1;

    char c;
    int letter_start;

    for(int i = 0; i < length - 1; i++)
    {
        letter_start = LOWERCASE;
        if(rand() % 2)
            letter_start = UPPERCASE;

        c = (rand() % 26) + letter_start;

        if(!(rand() % AVG_SPACE_GAP) && i != 0)
            c = ' ';

        *str = realloc(*str, sizeof(char) * str_size++);

        if(!(*str))
        {
            printf("Failed to reallocate memory for str!\n");
            exit(1);
        }

        (*str)[i] = c;
    }
    (*str)[length - 1] = '\0';
}

void AssignFitnessValues(Pool* pool)
{
    for(int i = 0; i < pool->num_entities; i++)
    {
        //for(int j = 0; j < strlen(pool->entities[i].sentence); j++)
        for(int j = 0; pool->entities[i].sentence[j] != '\0'; j++)
        {
            if( pool->entities[i].sentence[j] == TARGET[j] )
            {
                pool->entities[i].fitness_score++;
            }
        }
    }
}

int SelectSurvivors(Pool* pool, Entity** out_survivors)
{
    int num_survivors = 0;

    for(int i = 0; i < pool->num_entities; i++)
    {
        if( pool->entities[i].fitness_score >= pool->survivor_threash_hold )
        {
            (*out_survivors)[num_survivors].sentence = malloc(sizeof(char) * STRING_LENGTH);
            ValidateObject( (*out_survivors)[num_survivors].sentence, "(*out_survivors)[num_survivors].sentence" );

            (*out_survivors)[num_survivors].fitness_score = pool->entities[i].fitness_score;

            strcpy((*out_survivors)[num_survivors++].sentence, pool->entities[i].sentence);
            
            *out_survivors = realloc(*out_survivors, sizeof(Entity) * (num_survivors + 1));
            ValidateObject(*out_survivors, "*out_survivors");

            //printf("Survivor %i: %s\n", num_survivors, (*out_survivors)[num_survivors-1].sentence);
        }
    }

    // Compensate for the additional survivor
    if(num_survivors > 0)
    {
        *out_survivors = realloc(*out_survivors, sizeof(Entity) * (num_survivors)); 
        ValidateObject(*out_survivors, "*out_survivors");
    }

    return num_survivors;
}

int ReproduceSurvivors(Pool* pool)
{
    // 0 indicates no problems, we have a resulting pool that can continue on to the next generation
    int result = 0;

    Entity* survivors = malloc(sizeof(Entity)); 
    ValidateObject(survivors, "survivors");

    int num_survivors = SelectSurvivors(pool, &survivors);

    if(!num_survivors)
    {
        printf("The pool went extinct!\nNumber of generations: %i\n", pool->generation);
        // TODO: Do loop ending stuff here. Or have loop condition while num_survivors != 0? Maybe this function returns that?
        result = 2;
    }

    Pool* mating_pool = malloc(sizeof(Pool)); 
    ValidateObject(mating_pool, "mating_pool");

    // Not freeing due to how DeepCopy handles these... TODO: Maybe I should delete so that this is properly freed in same block it was allocated?
    mating_pool->entities = malloc(sizeof(Entity)); 
    ValidateObject(mating_pool->entities, "mating_pool->entities");

    mating_pool->num_entities = 0;
    mating_pool->survivor_threash_hold = pool->survivor_threash_hold + 1;
    mating_pool->survivor_rate = pool->survivor_rate;
    mating_pool->mutation_rate = pool->mutation_rate;
    mating_pool->generation = pool->generation + 1;

    if(num_survivors > 100)
        num_survivors = 80;
    
    printf("Survivors: %i\n", num_survivors);
    if(num_survivors > 1)
    {
        for(int i = 0; i < num_survivors; i++)
        {
            for(int j = i; j < num_survivors; j++)
            {
                if( (rand() % 100) < (PROB_MATE * 100) )
                {
                    // Ensure variability (don't mate with self :D)
                    if(i != j && ((rand() % 100) < (PROB_SUCC_REPRODUCTION * 100)))
                    {
                        mating_pool->entities = realloc(mating_pool->entities, sizeof(Entity) * ((++mating_pool->num_entities)));
                        ValidateObject(mating_pool->entities, "mating_pool->entities");

                        mating_pool->entities[mating_pool->num_entities - 1] = MateEntities(pool, &survivors[i], &survivors[j]);
                    }
                }
            }
        }
    }
    else if(num_survivors == 1)
    {
        printf("There are no other survivors to mate with!\n");
        printf("The single entity survivor will die alone...\n");
        result = 3;
    }

    DeepCopyPool(pool, mating_pool);
    
    free(mating_pool);

    return result;
}

Entity MateEntities(Pool* pool, Entity* e1, Entity* e2)
{
    Entity* child = malloc(sizeof(Entity));
    ValidateObject(child, "child");

    child->sentence = malloc(sizeof(char) * STRING_LENGTH);
    ValidateObject(child->sentence, "child->sentence");

    char c;
    int letter_start;

    child->fitness_score = 0;

    //printf("       : 0123456789012345678901234567890123456789\n");
    //printf("Target : %s\n", TARGET);
    //printf("Parent1: %s. Fitness: %i\n", e1->sentence, e1->fitness_score);
    //printf("Parent2 %s. Fitness: %i\n", e2->sentence, e2->fitness_score);

    //for(int i = 0; i < strlen(e1->sentence); i++)
    for(int i = 0; e1->sentence[i] != '\0'; i++)
    {
        // TODO: Maybe make it a random probability of whether it gets either parent's good genes
        if(e1->sentence[i] == TARGET[i])
        {
            //printf("_%c_", TARGET[i]);
            child->sentence[i] = e1->sentence[i];
            //printf("KEEPING: %c at %i\n", child->sentence[i], i);
            child->fitness_score++;
        }
        else if(e2->sentence[i] == TARGET[i])
        {
            //printf("_%c_", TARGET[i]);
            child->sentence[i] = e2->sentence[i];
            //printf("KEEPING: %c at %i\n", child->sentence[i], i);
            child->fitness_score++;
        }
        else
        {
            // Randomly select parent child will inherit this specific gene from (50/50)
            if(rand() % 2)
                child->sentence[i] = e1->sentence[i];
            else
                child->sentence[i] = e2->sentence[i];

            // TODO: Figure out a more concise and elegant way to do this conditional...
            // Chance of a mutation happening that will result in a random gene forming

            if( ((double) (rand() % 100)) < (pool->mutation_rate * 100))
            {
                letter_start = LOWERCASE;
                if(rand() % 2)
                    letter_start = UPPERCASE;

                c = (rand() % 26) + letter_start;

                if(!(rand() % AVG_SPACE_GAP) && i != 0)
                    c = ' ';

                child->sentence[i] = c;
            }
        }
    }
    //printf("Child: %s. Fitness: %i\n", child->sentence, child->fitness_score);

    return *child;
}

void DeepCopyPool(Pool* dest, Pool* source)
{
    //dest->num_entities = source->num_entities;
    dest->survivor_threash_hold = source->survivor_threash_hold;
    dest->survivor_rate = source->survivor_rate;
    dest->generation = source->generation;
    dest->mutation_rate = source->mutation_rate;

    dest->entities = realloc(dest->entities, sizeof(Entity));
    ValidateObject(dest->entities, "dest->entities");

    dest->num_entities = 0;
    for(int i = 0; i < source->num_entities; i++)
    {
        dest->entities = realloc(dest->entities, sizeof(Entity) * (++dest->num_entities));
        ValidateObject(dest->entities, "dest->entities");

        dest->entities[i] = source->entities[i];
    }

}

// TODO: This does more than just print the pool (also checks end condition)... Rename?
int PrintPool(Pool* pool, const char* name)
{
    int result = 0;
    int ideal_entity_index = -1;
    PrettyFormat();
    if(pool->num_entities > 0)
    {
        for(int i = 0; i < pool->num_entities; i++)
        {
            printf("Entity %i: %s\n", i+1, pool->entities[i].sentence);
            printf("With fitness: %i\n", pool->entities[i].fitness_score);
            if(pool->entities[i].fitness_score >= TARGET_FITNESS)
            {
                ideal_entity_index = i;
                result = 1;
            }
            printf("\n");
        }
    }
    else
        printf("Population has gone extinct\n", name);

    //printf("There are %i entities in the %s pool!\n", pool->num_entities, name);
    //printf("Survivor threash hold: %i\n", pool->survivor_threash_hold);

    //printf("Current Generation: %i\n", pool->generation);

    PrintPoolStatistics(pool, name);

    if( ideal_entity_index >= 0 )
        printf("\nResult: %s\n\n", pool->entities[ideal_entity_index].sentence);

    return result;
}

void PrintPoolStatistics(Pool* pool, const char* name)
{
    PrettyFormat();
    int lowest_fitness = 0;
    int highest_fitness = 0;
    int fitness_sum = 0;
    double average_fitness = 0;

    if(pool->num_entities > 0)
    {
        lowest_fitness = pool->entities[0].fitness_score;
        highest_fitness = lowest_fitness;

        for(int i = 0; i < pool->num_entities; i++)
        {
            int fitness_score = pool->entities[i].fitness_score;
            if(fitness_score > highest_fitness)
            {
                highest_fitness = fitness_score;
            }
            else if(fitness_score < lowest_fitness)
            {
                lowest_fitness = fitness_score;
            }
            fitness_sum += fitness_score;
        }
        average_fitness = fitness_sum / (double) pool->num_entities;
    }

    printf("\n");
    printf("There are %i entities in the %s pool!\n", pool->num_entities, name);
    printf("Survivor threash hold: %i\n", pool->survivor_threash_hold);
    printf("Current Generation: %i\n", pool->generation);
    printf("Average fitness: %0.1f\n", average_fitness);
    printf("Lowest fitness score: %i\n", lowest_fitness);
    printf("Highest fitness score: %i\n", highest_fitness);
    int run_time = (double) (clock() - START_TIME) / CLOCKS_PER_SEC;
    printf("Run time: %i seconds\n",  run_time);
}

void DestroyPool(Pool* pool)
{
    for(int i = 0; i < pool->num_entities; i++)
    {
        free(pool->entities[i].sentence);
    }

    free(pool->entities);

    free(pool);
}


























